import React from "react";
import '../styles/App.css';
import { useState } from "react";
import data from '../tasks.json'
import Header from "./Header";
import ToDoList from "./ToDoList";
import CreateTaskForm from "./CreateTaskForm";

const App = () => {

  const [toDoList, setToDoList] = useState(data);

  const handleToggle = (id) => {
    let mapped = toDoList.map(task => {
      return (
        task.id == id ? { ...task, complete: !task.complete } : { ...task }
      );
    });
    setToDoList(mapped);
  }

  const addTask = (userInput) => {
    let copy = [...toDoList];
    copy.push({ id: Math.random(), task: userInput, complete: false });
    setToDoList(copy);
  }

  const handleFilter = () => {
    let filterList = toDoList.filter(task => {
      return !task.complete;
    });
    setToDoList(filterList);
  }

  return (
    <div>
      <Header />
      <ToDoList toDoList={toDoList} handleToggle={handleToggle} handleFilter={handleFilter} />
      <CreateTaskForm addTask={addTask} />
    </div>
  );
};

export default App;