import React, { useState } from 'react';

const CreateTaskForm = ({ addTask }) => {
  const [userInput, setUserInput] = useState('');

  const handleChange = (event) => {
    setUserInput(event.currentTarget.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    addTask(userInput);
    setUserInput("");
  }

  return (
    <form onSubmit={handleSubmit}>
      <input value={userInput} type='text' onChange={handleChange} placeholder="Enter task name " />
      <button>Add task</button>
    </form>
  );
};

export default CreateTaskForm;