import React from "react";

const Header = () => {
  return (
    <h1 className="app-header">ToDo List</h1>
  );
};

export default Header;