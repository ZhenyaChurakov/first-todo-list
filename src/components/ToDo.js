import React from 'react';
import "../styles/ToDo.css";

const ToDo = ({ toDo, handleToggle }) => {

  const handleClick = (event) => {
    event.preventDefault();
    handleToggle(event.currentTarget.id);
  }

  return (
    <div id={toDo.id} key={toDo.id + toDo.task} name='todo' value={toDo.id} onClick={handleClick} className={toDo.complete ? "todo complete" : "todo"}>
      {toDo.task}
    </div>
  );
};

export default ToDo;