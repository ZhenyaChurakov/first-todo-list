import React from "react";
import ToDo from './ToDo';

const ToDoList = ({ toDoList, handleToggle, handleFilter }) => {
  return (
    <div className="app-list">
      {toDoList.map(toDo => {
        return (
          <ToDo key={toDo.id} toDo={toDo} handleToggle={handleToggle} handleFilter={handleFilter} />
        );
      })}
      <button onClick={handleFilter} className="clear">Clear</button>
    </div>
  );
};

export default ToDoList;